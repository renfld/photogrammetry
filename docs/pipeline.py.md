# Pipeline.py

Control whole pipeline by using one script (and command)

### Mandatory options:

```--input [directory]```

Directory where to read files (jpg, png, tif)

```--output [directory]```

Directory where to write OpenMVG and related files

```--type [global|incremental]```

Structure from Motion pipeline type.

### Optional:

```--openmvg [directory]```

Location of OpenMVG binaries. Default: [script_location]/../bin/openmvg

```--openmvs [directory]```

Location of OpenMVS binaries. Default: [script_location]/../bin/openmvs

```--debug```

Print commands instead of executing them

```--recompute```

Recompute everything

```--oopenmvs```

After going through OpenMVG pipeline, create a OpenMVS project

```--omeshlab```

After going through OpenMVG pipeline, create a Meshlab project

### Pipeline options

#### Image Listing:

```--cgroup```

Each view have it's own camera intrinsic parameters

```--flength [float]```

If your camera is not listed in the camera sensor database, you can set pixel focal length here. The value can be calculated by max(width-pixels, height-pixels) * focal length(mm) / Sensor width (mm).

```--cmodel [int]```

##### Camera model:

1. Pinhole
2. Pinhole Radial 1
3. Pinhole Radial 3 (default)

#### Compute Features:

```--dpreset [string]```

Used to control the Image_describer configuration:

* ```NORMAL```
* ```HIGH```
* ```ULTRA```

#### Compute Matches:

```--ratio [float]```

Nearest Neighbor distance ratio (smaller is more restrictive => Less false positives). Default: 0.8

```--geomodel [char]```

##### Compute Matches geometric model:
* f: Fundamental matrix filtering (default) (Incremental SfM)
* e: Essential matrix filtering (Global SfM)
* h: Homography matrix filtering

```--nmatching [string]```

##### Compute Matches Nearest Matching Method:

* ```BRUTEFORCEL2```: BruteForce L2 matching for Scalar based regions descriptor,
* ```ANNL2```: Approximate Nearest Neighbor L2 matching for Scalar based regions descriptor,
* ```CASCADEHASHINGL2```: L2 Cascade Hashing matching,
* ```FASTCASCADEHASHINGL2```: (default)

#### Incremental SfM


```--icmodel [int]```

The camera model type that will be used for views with unknown intrinsic:

1. Pinhole
2. Pinhole radial 1
3. Pinhole radial 3 (default)
4. Pinhole radial 3 + tangential 2
5. Pinhole fisheye

#### Global SfM

```--grotavg [int]```
1. L1 rotation averaging [Chatterjee]
2. L2 rotation averaging [Martinec] (default)

```--gtransavg [int]```
1. L1 translation averaging [GlobalACSfM]
2. L2 translation averaging [Kyle2014]
3. SoftL1 minimization [GlobalACSfM] (default)


#### OpenMVS




